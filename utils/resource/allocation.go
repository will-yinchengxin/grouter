package resource

//func Resource() {
//	numMachines := 5
//	numResourcesPerMachine := 2
//	totalResources := numMachines * numResourcesPerMachine
//
//	var wg sync.WaitGroup
//	wg.Add(numMachines)
//
//	// 创建一个长度为 15 的切片，初始值为 0，表示每个机器没有被分配资源
//	allocatedResources := make([]int, numMachines)
//
//	for i := 0; i < totalResources; i++ {
//		// 找到已经分配最少资源的机器
//		minIndex := 0
//		for j := 1; j < numMachines; j++ {
//			if allocatedResources[j] < allocatedResources[minIndex] {
//				minIndex = j
//			}
//		}
//
//		// 分配一个资源给这个机器
//		allocatedResources[minIndex]++
//
//		// 打印分配的资源和每个机器分配的资源总数
//		fmt.Printf("资源 %d 分配给机器 %d，分配的资源总数：%v\n", i+1, minIndex+1, allocatedResources)
//
//		// 启动一个 goroutine 模拟机器的资源使用
//		go func(machine int) {
//			defer wg.Done()
//
//			// do something with resource
//
//			// 释放一个资源
//			allocatedResources[machine]--
//		}(minIndex)
//
//		// 动态调整已经分配的资源数量
//		for j := 0; j < numMachines; j++ {
//			if allocatedResources[j] < allocatedResources[minIndex] {
//				// 重新分配资源给之前已经分配最少资源的机器
//				allocatedResources[minIndex]--
//				allocatedResources[j]++
//				// 打印动态调整后的资源分配情况
//				fmt.Printf("动态调整：资源从机器 %d 分配到机器 %d，分配的资源总数：%v\n", minIndex+1, j+1, allocatedResources)
//				break
//			}
//		}
//	}
//
//	wg.Wait()
//}

//var (
//	UStatusMap = make(map[string]UserStatus)
//	UStatusMapLock sync.RWMutex
//)

// 用户状态
//type UserStatus struct {
//	LastHeartbeat time.Time // 最后一次心跳时间戳
//	// 其他状态信息
//}

//func HearBeat() {
//	// 维护的用户请求状态
//	userStatuses := make(map[string]UserStatus)
//	// 互斥锁，保证并发安全
//	mutex := sync.Mutex{}
//
//	// 模拟心跳请求
//	go func() {
//		for {
//			// 随机选择一个用户
//			userId := fmt.Sprintf("user_%d", math.Round(3000)))
//
//			mutex.Lock()
//			// 更新用户状态的最后一次心跳时间戳
//			userStatus := userStatuses[userId]
//			userStatus.LastHeartbeat = time.Now()
//			userStatuses[userId] = userStatus
//			mutex.Unlock()
//
//			// 模拟发送心跳请求
//			time.Sleep(500 * time.Millisecond)
//		}
//	}()
//
//	// 定时检查用户状态
//	go func() {
//		for {
//			// 检查每个用户的状态
//			mutex.Lock()
//			for userId, userStatus := range userStatuses {
//				// 如果超过 60 秒没有心跳，则删除该用户的状态信息
//				if time.Since(userStatus.LastHeartbeat) > 60*time.Second {
//					delete(userStatuses, userId)
//					fmt.Printf("User %s status deleted\n", userId)
//				}
//			}
//			mutex.Unlock()
//
//			// 等待一段时间后再检查
//			time.Sleep(10 * time.Second)
//		}
//	}()
//
//	// 防止程序退出
//	select {}
//}

//func main() {
//	numMachines := 15
//	numResourcesPerMachine := 200
//	totalResources := numMachines * numResourcesPerMachine
//
//	// 连接 Redis
//	conn, err := redis.Dial("tcp", "localhost:6379")
//	if err != nil {
//		panic(err)
//	}
//	defer conn.Close()
//
//	var result interface{}
//	// 设置分布式锁，避免资源重复分配
//	for i := 0; i < totalResources; i++ {
//		// 请求分布式锁
//		result, err = conn.Do("SET", "resource_lock", "1", "EX", "10", "NX")
//		if err != nil {
//			panic(err)
//		}
//		if result == nil {
//			// 分布式锁被其他节点占用，等待一段时间后重新请求锁
//			time.Sleep(100 * time.Millisecond)
//			i--
//			continue
//		}
//
//		// 找到已经分配最少资源的机器
//		minIndex := 0
//		for j := 1; j < numMachines; j++ {
//			if allocatedResources[j] < allocatedResources[minIndex] {
//				minIndex = j
//			}
//		}
//
//		// 分配一个资源给这个机器
//		allocatedResources[minIndex]++
//
//		// 打印分配的资源和每个机器分配的资源总数
//		fmt.Printf("资源 %d 分配给机器 %d，分配的资源总数：%v\n", i+1, minIndex+1, allocatedResources)
//
//		// 启动一个 goroutine 模拟机器的资源使用
//		go func(machine int) {
//			defer wg.Done()
//
//			// do something with resource
//
//			// 释放一个资源
//			allocatedResources[machine]--
//
//			// 释放分布式锁
//			_, err = conn.Do("DEL", "resource_lock")
//			if err != nil {
//				panic(err)
//			}
//		}(minIndex)
//	}
//
//	wg.Wait()
//}
