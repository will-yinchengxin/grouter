package trans

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"strconv"
	response2 "will/app/do/response"
	"will/consts"
)

func MergeInfo(body response2.TransResRemote) response2.TransRep {
	// 拼接信息规则
	// {"urlData":"{Base64(mqttIp=xxx&mqttPort=&mqttUsername=admin&token=xxx&rtspPlayUrl=rtsp://admin:e52dc71b-5b4c-48b6-b034-cb0f168b2b0c@172.16.246.10:31200/desktop)}"

	// mqttIP: 原ip
	// mqttPort: 原 port + 1
	// mqttUserName：admin
	// token：原token
	// rtspPlayurl： rtsp://admin:token@原ip:原port：/desktop ｜｜ rtsp://admin:e52dc71b-5b4c-48b6-b034-cb0f168b2b0c@172.16.246.10:31200/desktop
	mqttIp := body.Model.CloudInstanceInfo.IP
	mqttPort := body.Model.CloudInstanceInfo.Port1 + 1
	mqttUserName := consts.TranUser
	token := body.Model.CloudInstanceInfo.Token

	var tmpStr bytes.Buffer
	tmpStr.WriteString("rtsp://")
	tmpStr.WriteString(consts.TranUser)
	tmpStr.WriteString(":")
	tmpStr.WriteString(token)
	tmpStr.WriteString("@")
	tmpStr.WriteString(mqttIp)
	tmpStr.WriteString(":")
	tmpStr.WriteString(strconv.Itoa(body.Model.CloudInstanceInfo.Port1))
	tmpStr.WriteString("/desktop")
	rtspPlayurl := tmpStr.String()

	var tmpStrAno bytes.Buffer
	tmpStrAno.WriteString("mqttIp=")
	tmpStrAno.WriteString(mqttIp)
	tmpStrAno.WriteString("&mqttPort=")
	tmpStrAno.WriteString(strconv.Itoa(mqttPort))
	tmpStrAno.WriteString("&mqttUsername=")
	tmpStrAno.WriteString(mqttUserName)
	tmpStrAno.WriteString("&token=")
	tmpStrAno.WriteString(token)
	tmpStrAno.WriteString("&rtspPlayUrl=")
	tmpStrAno.WriteString(rtspPlayurl)
	urlD := tmpStrAno.String()
	fmt.Println("\r\n ===> ", urlD, "<===>", base64.StdEncoding.EncodeToString([]byte(urlD)), " <=== \r\n")

	// base64.StdEncoding.EncodeToString([]byte(urlD))
	// base64.StdEncoding.DecodeString([]byte(urlD))
	return response2.TransRep{
		UrlData:           base64.StdEncoding.EncodeToString([]byte(urlD)),
		CloudInstanceInfo: body.Model.CloudInstanceInfo,
	}
}
