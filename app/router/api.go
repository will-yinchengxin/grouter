package router

import (
	"github.com/gin-gonic/gin"
	"will/app/controller"
)

type ApiRouter struct {
	AppsApi *controller.Apps
}

func (api *ApiRouter) Initialize(app *gin.Engine) {
	v1 := app.Group("api")
	{
		apps := v1.Group("v1")
		{
			apps.GET("/trans/transApi", api.AppsApi.TransApi)
			apps.GET("/trans/heartBeat", api.AppsApi.HeartBeat)
		}
	}
}
