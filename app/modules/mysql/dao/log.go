package dao

import (
	"context"
	"will/app/modules/mysql/entity"
)

type Log struct {
	*MysqlPool
}

func (l *Log) WithContext(ctx context.Context) *Log {
	l.DB = l.DB.WithContext(ctx)
	return l
}

func (l *Log) Insert(req entity.Log) error {
	return l.DB.Model(entity.Log{}).
		Create(&req).Error
}
