package dao

import (
	"context"
	"will/app/modules/mysql/entity"
	"will/consts"
)

type Resource struct {
	*MysqlPool
}

func (r *Resource) WithContext(ctx context.Context) *Resource {
	r.DB = r.DB.WithContext(ctx)
	return r
}

func (r *Resource) Add(token, typeStr, sessionId string) error {
	info := &entity.Resource{}
	if typeStr == consts.StopIns {
		return r.DB.Model(entity.Resource{}).Where("token = ? AND session_id = ?", token, sessionId).Delete(*info).Error
	}
	info.Token = token
	info.SessionId = sessionId
	err := r.DB.Model(entity.Resource{}).Create(info).Error
	return err

	//return r.DB.Exec("INSERT INTO `resource` (`id`,`num`) VALUES (1,?) ON DUPLICATE KEY UPDATE `num` = CASE WHEN `num` + VALUES(`num`) >= 0 AND `num` + VALUES(`num`) < ? THEN `num` + VALUES(`num`) ELSE `num` END", number, total).Error
}

func (r *Resource) CheckResource(numResources int64) (bool, error) {
	var count int64
	err := r.DB.Model(entity.Resource{}).
		Count(&count).Error
	if err != nil {
		return false, err
	}
	return count <= numResources, nil
}

func (r *Resource) Delete(token, sessionID string) error {
	return r.DB.Model(entity.Resource{}).
		Where("token = ? AND session_id = ?", token, sessionID).
		Delete(&entity.Resource{}).
		Error
}
