package dao

import (
	"context"
	"encoding/json"
	"strconv"
	"time"
	requestParam "will/app/do/request"
	response2 "will/app/do/response"
	"will/app/modules/mysql/entity"
	"will/consts"
	"will/utils/trans"
)

type ClientHeartBeat struct {
	*MysqlPool
}

func (c *ClientHeartBeat) WithContext(ctx context.Context) *ClientHeartBeat {
	c.DB = c.DB.WithContext(ctx)
	return c
}

func (c *ClientHeartBeat) GetList(offset, batchSize int) ([]entity.ClientHeartBeat, error) {
	db := c.DB.Model(entity.ClientHeartBeat{})
	list := []entity.ClientHeartBeat{}
	err := db.Scopes(PaginateParams(offset, batchSize)).
		Find(&list).Error
	return list, err
}

func (c *ClientHeartBeat) GetCount() (int64, error) {
	var count int64
	err := c.DB.Model(entity.ClientHeartBeat{}).Count(&count).Error
	return count, err
}

func (c *ClientHeartBeat) Release(req requestParam.TransReq) error {
	return c.DB.Model(entity.ClientHeartBeat{}).
		Where("uuid = ? AND user_id = ? AND session_id = ?", req.UUID, req.UserID, req.SessionID).
		Delete(&entity.ClientHeartBeat{}).
		Error
}

func (c *ClientHeartBeat) Delete(id int64) error {
	return c.DB.Model(entity.ClientHeartBeat{}).
		Where("id = ?", id).
		Delete(&entity.ClientHeartBeat{}).
		Error
}

func (c *ClientHeartBeat) Update(data entity.ClientHeartBeat, t int64) error {
	return c.DB.Model(entity.ClientHeartBeat{}).
		Where("id = ?", data.ID).
		Update("create_time", t).
		Error
}

func (c *ClientHeartBeat) SearchStatus(cbModel *entity.ClientHeartBeat, req requestParam.HeartBeatReq) error {
	return c.DB.Model(entity.ClientHeartBeat{}).
		Where("uuid = ? AND user_id = ? AND session_id = ?", req.UUID, req.UserID, req.SessionID).
		Select("uuid", "user_id", "status", "session_id").
		Find(&cbModel).Error
}

func (c *ClientHeartBeat) ReturnOrCreate(req requestParam.TransReq) (entity.ClientHeartBeat, error) {
	var (
		err      error
		chbModel = entity.ClientHeartBeat{}
	)
	tx := c.DB.Begin()
	defer tx.Rollback()

	if err = c.DB.Model(entity.ClientHeartBeat{}).
		Where("uuid = ? AND user_id = ? AND session_id = ?", req.UUID, req.UserID, req.SessionID).
		Find(&chbModel).Error; err != nil {
		return chbModel, err
	}
	now := time.Now().Unix()
	if chbModel.ID > 0 {
		if now-chbModel.CreateTime > consts.TimeInterVal {
			err = c.DB.Model(entity.ClientHeartBeat{}).
				Where("id = ?", chbModel.ID).
				Delete(&entity.ClientHeartBeat{}).
				Error
			if err != nil {
				return entity.ClientHeartBeat{}, err
			}
			chbModel.ID = 0
			chbModel.SucData = chbModel.SucData
			chbModel.SessionId = chbModel.SessionId
		}
		tx.Commit()
		return chbModel, nil
	}
	chbModel.Uuid = req.UUID
	chbModel.Userid = strconv.Itoa(req.UserID)
	chbModel.ClientAppVersion = req.ClientAppVersion
	chbModel.SessionId = req.SessionID
	chbModel.ApiName = req.ApiName
	chbModel.ResourceId = req.ResourceId
	chbModel.CreateTime = now
	chbModel.SucData = ""
	chbModel.Status = 0
	chbModel.AppPackage = req.AppPackage
	if err = c.DB.Model(entity.ClientHeartBeat{}).
		Create(&chbModel).Error; err != nil {
		return entity.ClientHeartBeat{}, err
	}
	tx.Commit()
	return chbModel, nil
}

func (c *ClientHeartBeat) UpdateOrInsert(req requestParam.TransReq, remoteRes response2.TransResRemote) (string, error) {
	var (
		err     error
		urlData string
	)
	tx := c.DB.Begin()
	defer tx.Rollback()
	chbModel := entity.ClientHeartBeat{}
	if err = c.DB.Model(entity.ClientHeartBeat{}).
		Where("uuid = ? AND user_id = ? AND session_id = ?", req.UUID, req.UserID, req.SessionID).
		Find(&chbModel).
		Error; err != nil {
		return "", err
	}

	if chbModel.ID > 0 {
		// 记录存在
		// 为 StopInstance 状态
		if req.ApiName == consts.StopIns {
			tmp := response2.TransRep{}
			json.Unmarshal([]byte(chbModel.SucData), &tmp)
			urlData = tmp.UrlData
			err = c.DB.Model(entity.ClientHeartBeat{}).
				Where("id = ?", chbModel.ID).
				Delete(&entity.ClientHeartBeat{}).
				Error
			if err != nil {
				return "", err
			}
		} else {
			strSunData := trans.MergeInfo(remoteRes)
			strSunDataByte, _ := json.Marshal(strSunData)
			urlData = strSunData.UrlData
			chbModelNew := &entity.ClientHeartBeat{
				SucData:    string(strSunDataByte),
				Status:     1,
				CreateTime: time.Now().Unix(),
			}
			err = c.DB.Model(entity.ClientHeartBeat{}).
				Where("id = ?", chbModel.ID).
				Select("suc_data", "status", "create_time").
				Updates(chbModelNew).Error
			if err != nil {
				return "", err
			}
		}
	} else {
		// 记录不存在
		// 为 RunInstance 状态
		if req.ApiName == consts.RunIns {
			strSunData := trans.MergeInfo(remoteRes)
			strSunDataByte, _ := json.Marshal(strSunData)
			urlData = strSunData.UrlData
			chbModelNew := &entity.ClientHeartBeat{
				Uuid:             req.UUID,
				Userid:           strconv.Itoa(req.UserID),
				ClientAppVersion: req.ClientAppVersion,
				SessionId:        req.SessionID,
				ApiName:          req.ApiName,
				ResourceId:       req.ResourceId,
				SucData:          string(strSunDataByte),
				Status:           1,
				CreateTime:       time.Now().Unix(),
				AppPackage:       req.AppPackage,
			}
			err = c.DB.Model(entity.ClientHeartBeat{}).
				Create(chbModelNew).Error
			if err != nil {
				return "", err
			}
		}
	}
	tx.Commit()
	return urlData, nil
}
