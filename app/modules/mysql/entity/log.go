package entity

type Log struct {
	ID        int64  `gorm:"column:id" json:"id" form:"id"`
	ReqTime   int64  `gorm:"column:req_time" json:"req_time" form:"req_time"`
	ReqType   string `gorm:"column:req_type" json:"req_type" form:"req_type"`
	ReqParam  string `gorm:"column:req_param" json:"req_param" form:"req_param"`
	RetStatus int64  `gorm:"column:ret_status" json:"ret_status" form:"ret_status"`
	RetMsg    string `gorm:"column:ret_msg" json:"ret_msg" form:"ret_msg"`
}

func (l *Log) TableName() string {
	return "log"
}
