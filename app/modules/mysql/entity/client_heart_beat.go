package entity

type ClientHeartBeat struct {
	ID               int64  `gorm:"column:id" json:"id" form:"id"`
	Uuid             string `gorm:"column:uuid" json:"uuid" form:"uuid"`
	Userid           string `gorm:"column:user_id" json:"userId" form:"userid"`
	ClientAppVersion string `gorm:"column:client_app_version" json:"client_app_version" form:"client_app_version"`
	SessionId        string `gorm:"column:session_id" json:"session_id" form:"session_id"`
	ApiName          string `gorm:"column:api_name" json:"api_name" form:"api_name"`
	ResourceId       string `gorm:"column:resource_id" json:"resource_id" form:"resource_id"`
	CreateTime       int64  `gorm:"column:create_time" json:"create_time" form:"create_time"`
	SucData          string `gorm:"column:suc_data" json:"suc_data" form:"suc_data"`
	Status           int64  `gorm:"column:status" json:"status" form:"status"`
	AppPackage       string `gorm:"column:app_package" json:"app_package" form:"app_package"`
}

func (c *ClientHeartBeat) TableName() string {
	return "client_heart_beat"
}
