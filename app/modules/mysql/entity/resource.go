package entity

type Resource struct {
	ID        int64  `gorm:"column:id" json:"id" form:"id"`
	Token     string `gorm:"column:token" json:"token" form:"token"`
	SessionId string `gorm:"column:session_id" json:"session_id" form:"session_id"`
}

func (l *Resource) TableName() string {
	return "resource"
}
