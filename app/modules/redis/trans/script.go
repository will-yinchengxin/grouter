package trans

const (
	HeartBeatCommand = `
local key = KEYS[1]
local value = ARGV[1]

if redis.call('EXISTS', key) == 1 then
	redis.call('SETEX', key, 120, value)
    return 0
else
    redis.call('SET', key, value)
    redis.call('EXPIRE', key, 120)
    return 1
end
`
)
