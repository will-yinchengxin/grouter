package service

import (
	"bytes"
	"context"
	"crypto/hmac"
	"crypto/md5"
	"crypto/rand"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	aliAuth "github.com/aliyun/alibaba-cloud-sdk-go/sdk/auth"
	"github.com/aliyun/alibaba-cloud-sdk-go/sdk/auth/credentials"
	"github.com/aliyun/alibaba-cloud-sdk-go/sdk/auth/signers"
	"github.com/aliyun/alibaba-cloud-sdk-go/sdk/requests"
	aliUtils "github.com/aliyun/alibaba-cloud-sdk-go/sdk/utils"
	"github.com/jinzhu/copier"
	"hash"
	"io/ioutil"
	rand2 "math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
	requestParam "will/app/do/request"
	response2 "will/app/do/response"
	"will/app/modules/mysql/dao"
	"will/app/modules/mysql/entity"
	conn "will/app/modules/redis"
	"will/app/modules/redis/trans"
	"will/consts"
	"will/core"
	"will/utils"
)

type Apps struct {
	Rds             *conn.RedisPool
	User            *dao.User
	Log             *dao.Log
	Resource        *dao.Resource
	ClientHeartBeat *dao.ClientHeartBeat
}

var (
	hookGetDate = func(fn func() string) string {
		return fn()
	}
	hookGetNonce = func(fn func() string) string {
		return fn()
	}
)

// HeartBeat 客户端心跳上报
func (a *Apps) HeartBeat(ctx context.Context, req requestParam.HeartBeatReq) (codeType *utils.CodeType) {
	var (
		err error
	)
	defer func() {
		utils.ErrorLog(err)
	}()
	evalRes, err := a.Rds.EvalCtx(ctx, trans.HeartBeatCommand,
		[]string{req.UUID + req.SessionID}, time.Now().Unix())
	if err != nil {
		return utils.HeartBeatFail
	}
	if evalRes.(int64) == 0 {
		_ = core.Log.InfoDefault("Heart beat exit, set new expire")
	} else {
		_ = core.Log.InfoDefault("Set new heart beat")
	}

	return &utils.CodeType{}
}

// TransApi 申请 / 释放资源
func (a *Apps) TransApi(ctx context.Context, req requestParam.TransReq) (interface{}, *utils.CodeType) {
	var (
		err error
	)
	defer func() {
		utils.ErrorLog(err)
	}()
	// 校验资源是否充足
	if req.ApiName == consts.RunIns && !a.checkResource() {
		return "", utils.ResourceExhaust
	}

	// heartbeat 无记录加入记录，有记录未超时直接返回资源， 有记录且超时直接删除
	if req.ApiName == consts.RunIns {
		resource, err := a.ClientHeartBeat.ReturnOrCreate(req)
		if err != nil {
			core.Log.ErrorDefault("ClientHeartBeat.ReturnOrCreate err" + err.Error())
			return nil, utils.DBError
		}
		if resource.ID == 0 && len(resource.SucData) > 0 {
			tmp := response2.TransRep{}
			json.Unmarshal([]byte(resource.SucData), &tmp)
			a.Resource.Delete(tmp.UrlData, resource.SessionId)
			// 释放资源
			tmpReq := requestParam.TransReq{}
			copier.Copy(&tmpReq, &req)
			tmpReq.ApiName = consts.StopIns
			_, err = PrepareTransApi(tmpReq)
			core.Log.ErrorDefault("TmpReq PrepareTransApi StopInstance" + err.Error())
			time.Sleep(time.Millisecond * 5)
		}
		if resource.ID > 0 && resource.Status == 1 {
			resRes := &response2.TransRep{}
			json.Unmarshal([]byte(resource.SucData), resRes)
			return resRes, &utils.CodeType{}
		}
	}
	// 可能 启动/停止，body 不一致
	lModel := a.Log.WithContext(ctx)
	logInfo, _ := json.Marshal(req)
	log := entity.Log{
		ReqTime:  time.Now().Unix(),
		ReqParam: string(logInfo),
		ReqType:  req.ApiName,
	}
	body, err := PrepareTransApi(req)
	if err != nil {
		log.RetStatus = consts.ReqFail
		log.RetMsg = err.Error()
		lModel.Insert(log)
		return "", utils.GetResourceFail
	}
	log.RetStatus = consts.ReqSuc
	log.RetMsg = ""
	lModel.Insert(log)

	urlData, err := a.ClientHeartBeat.UpdateOrInsert(req, body)
	if err = a.Resource.Add(urlData, req.ApiName, req.SessionID); err != nil {
		core.Log.ErrorDefault("Resource.Del err:" + err.Error())
		return nil, utils.DBError
	}
	if req.ApiName == consts.StopIns {
		a.ClientHeartBeat.Release(req)
	}
	res := response2.TransRep{
		UrlData:           urlData,
		CloudInstanceInfo: body.Model.CloudInstanceInfo,
	}

	return res, &utils.CodeType{}
}

func (a *Apps) checkResource() bool {
	var (
		NumMachines            = core.CoreConfig["nummachines"].(int)
		NumResourcesPerMachine = core.CoreConfig["numresourcespermachine"].(int)
	)
	ok, err := a.Resource.CheckResource(int64(NumMachines * NumResourcesPerMachine))
	if err != nil {
		core.Log.ErrorDefault("CheckResource: " + err.Error())
	}
	return ok
}

func PrepareTransApi(req requestParam.TransReq) (response2.TransResRemote, error) {
	var (
		urlData         string
		AccessKeyId     = core.CoreConfig["accesskeyid"].(string)
		AccessKeySecret = core.CoreConfig["accesskeysecret"].(string)
		ReqUrl          = core.CoreConfig["transapiurl"].(string)
		Domain          = core.CoreConfig["transapidomain"].(string)
		Scheme          = core.CoreConfig["transapischeme"].(string)
	)
	/*
		1.1
			【删除】去除对 TenantId、Env 的支持
			【新增】resourceId 指定资源调度
		1.2
			【新增】调度资源参数 ResourceInfo
				原 resourceId 从 ExtInfo 移入，即 ResourceId
				新增支持 nodeId 指定，即 NodeId
			【删除】
				ClientInfo 中 ClientAppVersion 参数，缺省支持

		{
			"ClientInfo":{"ClientAppVersion":"169","UserId":"123","ClientIp":"","Uuid":"gh1372392627"},
			"ExtInfo":{"queueUpConfirmFlag":"true","deviceSpec":"spec_openapi","resourceId":"","continueTrialPlayFlag":"false","disableAppNasPathFlag":"true","customizedProperties":{"sys.aaa.userid":"gh1372392627","sys.wired.macaddr":"gb1372392627"}},
			"AppLaunchInfo": {"AppPackage":"com.xormedia.cloudst.reamagent","AppActivityName":"com-xormedia.cloudstreamagent.MainActivity","AppServiceName":"","AppParams":{"deviceid":"gh1372392627"}},"MonitorInfo": {"Category":"", "Type":"","Context":""},
			"SessionId":"gh1372392627011T142109161965"
		}
	*/
	//urlData = fmt.Sprintf("{\"AppLaunchInfo\":{\"AppVersion\":\"\",\"AppPackage\":\"%s\"},\"ClientInfo\":{\"Uuid\":\"%s\",\"UserId\":%d,\"ClientAppVersion\":\"%s\"},\"SessionId\":\"%s\",\"ExtInfo\":{\"queueUpConfirmFlag\":true,\"deviceSpec\":\"spec_openapi\",\"resourceId\":\"%s\"}}", req.AppPackage, req.UUID, req.UserID, req.ClientAppVersion, req.SessionID, req.ResourceId)
	// v1 urlData = fmt.Sprintf("{\"ClientInfo\":{\"ClientAppVersion\":\"%s\",\"UserId\":\"%d\",\"ClientIp\":\"\",\"Uuid\":\"%s\"},\"ExtInfo\":{\"queueUpConfirmFlag\":\"true\",\"deviceSpec\":\"spec_openapi\",\"resourceId\":\"%s\"},\"AppLaunchInfo\": {\"AppPackage\":\"%s\",\"AppActivityName\":\"%s\",\"AppServiceName\":\"\",\"AppParams\":{\"deviceid\":\"%s\"}},\"MonitorInfo\": {\"Category\":\"\", \"Type\":\"\",\"Context\":\"\"},\"SessionId\":\"%s\"}", req.ClientAppVersion, req.UserID, req.UUID, req.ResourceId, req.AppPackage, req.AppActivityName, req.UUID, req.SessionID)
	// v2 urlData = fmt.Sprintf("{\"ClientInfo\":{\"ClientAppVersion\":\"%s\",\"UserId\":\"%d\",\"ClientIp\":\"\",\"Uuid\":\"%s\"},\"ExtInfo\":{\"queueUpConfirmFlag\":\"true\",\"deviceSpec\":\"spec_openapi\",\"resourceId\":\"%s\",\"customizedProperties\":{\"sys.aaa.userid\":\"%s\",\"sys.wired.macaddr\":\"%s\"}},\"AppLaunchInfo\": {\"AppPackage\":\"%s\",\"AppActivityName\":\"%s\",\"AppServiceName\":\"\",\"AppParams\":{\"deviceid\":\"%s\"}},\"MonitorInfo\": {\"Category\":\"\", \"Type\":\"\",\"Context\":\"\"},\"SessionId\":\"%s\"\n\t\t}", req.ClientAppVersion, req.UserID, req.UUID, req.ResourceId, req.UUID, req.UUID, req.AppPackage, req.AppActivityName, req.UUID, req.SessionID)
	// v4 urlData = fmt.Sprintf("{\"ClientInfo\":{\"ClientAppVersion\":\"%s\",\"UserId\":\"%d\",\"ClientIp\":\"183.242.72.21\",\"Uuid\":\"%s\"},\"ExtInfo\":{\"env\":\"production\",\"queueUpConfirmFlag\":\"true\",\"resourceId\":\"%s\",\"continueTrialPlayFlag\":\"false\",\"disableAppNasPathFlag\":\"true\"},\"AppLaunchInfo\": {\"AppPackage\":\"%s\",\"AppActivityName\":\"%s\",\"AppServiceName\":\"\",\"AppParams\":{\"deviceid\":\"%s\"}},\"MonitorInfo\": {\"Category\":\"\", \"Type\":\"\",\"Context\":\"\"},\"SessionId\":\"%s\"\n\t\t}", req.ClientAppVersion, req.UserID, req.UUID, req.ResourceId, req.AppPackage, req.AppActivityName, req.UUID, req.SessionID)
	urlData = fmt.Sprintf("{\"ClientInfo\":{\"ClientAppVersion\":\"%s\",\"UserId\":\"%d\",\"ClientIp\":\"\",\"Uuid\":\"%s\"},\"ExtInfo\":{\"env\":\"production\",\"queueUpConfirmFlag\":\"true\",\"resourceId\":\"%s\",\"continueTrialPlayFlag\":\"false\",\"disableAppNasPathFlag\":\"true\"},\"AppLaunchInfo\": {\"AppPackage\":\"%s\",\"AppActivityName\":\"%s\",\"AppServiceName\":\"\",\"AppParams\":{\"deviceid\":\"%s\"}},\"MonitorInfo\": {\"Category\":\"\", \"Type\":\"\",\"Context\":\"\"},\"SessionId\":\"%s\"\n\t\t}", req.ClientAppVersion, req.UserID, req.UUID, req.ResourceId, req.AppPackage, req.AppActivityName, req.UUID, req.SessionID)
	// fmt.Println("\r\n urlData", urlData, "\r\n")

	reqTransApi := requestParam.TransReqToApi{
		Url:             ReqUrl,
		Method:          http.MethodGet,
		Domain:          Domain,
		ApiName:         req.ApiName,
		Version:         "3999-99-99",
		Scheme:          Scheme,
		FormData:        "",
		UrlData:         urlData,
		AccessKeyId:     AccessKeyId,
		AccessKeySecret: AccessKeySecret,
	}
	return TransApi(reqTransApi)
}

func TransApi(req requestParam.TransReqToApi) (response2.TransResRemote, error) {
	var (
		err      error
		res      = &response2.TransResRemote{}
		urlData  = make(map[string]interface{})
		formData = make(map[string]interface{})
	)
	if len(req.UrlData) > 0 {
		err = json.Unmarshal([]byte(req.UrlData), &urlData)
	}

	if err != nil {
		return *res, err
	}

	if len(req.FormData) > 0 {
		err = json.Unmarshal([]byte(req.FormData), &formData)
	}

	if err != nil {
		return *res, err
	}

	if len(req.AccessKeyId) <= 0 {
		req.AccessKeyId = req.AccessKeyId
	}

	if len(req.AccessKeySecret) <= 0 {
		req.AccessKeySecret = req.AccessKeySecret
	}
	//构建公共参数
	c := credentials.NewAccessKeyCredential(req.AccessKeyId, req.AccessKeySecret)
	signer := signers.NewAccessKeySigner(c)
	request := requests.NewCommonRequest()
	request.Scheme = req.Scheme
	request.Domain = req.Domain
	request.Method = req.Method
	request.ApiName = req.ApiName
	request.Version = req.Version
	request.TransToAcsRequest()
	//组装请求数据

	for k, v := range urlData {
		k := k
		v := v
		request.QueryParams[k] = Strval(v)

	}
	for k, v := range formData {
		k := k
		v := v
		request.FormParams[k] = Strval(v)
	}

	//设置参数
	err = completeRpcSignParams(request, signer, "")
	if err != nil {
		return *res, err
	}
	// remove while retry
	if _, containsSign := request.GetQueryParams()["Signature"]; containsSign {
		delete(request.GetQueryParams(), "Signature")
	}

	//计算签名
	stringToSign := buildRpcStringToSign(request)
	request.SetStringToSign(stringToSign)
	//secret := "0jrYjV9cHUpVl6rKIY8Fcz0rion7QG" + "&"
	secret := req.AccessKeySecret + "&"
	signature := ShaHmac1(stringToSign, secret)
	request.GetQueryParams()["Signature"] = signature
	signature = strings.Replace(signature, "+", "%20", -1)
	signature = strings.Replace(signature, "*", "%2A", -1)
	signature = strings.Replace(signature, "%7E", "~", -1)
	signature = url.QueryEscape(signature)
	params := url.Values{}
	Url, err := url.Parse(req.Url)
	if err != nil {
		return *res, err
	}
	for k, v := range request.QueryParams {
		params.Set(k, v)
	}
	Url.RawQuery = params.Encode()
	urlPath := Url.String()
	// 获取 request请求
	request1, err := http.NewRequest(req.Method, urlPath, bytes.NewReader(request.Content))
	if err != nil {
		return *res, err
	}
	client := &http.Client{}
	response, err := client.Do(request1)
	defer response.Body.Close()
	body, err := ioutil.ReadAll(response.Body)
	fmt.Println("\r\n GET Remote Body: ", string(body), time.Now().Unix(), "\n\r")
	if err != nil {
		return *res, err
	}

	if err = json.Unmarshal(body, res); err != nil {
		return *res, err
	}
	if !res.Success {
		return response2.TransResRemote{}, errors.New("Get Remote Body Fail: " + req.ApiName + " " + req.UrlData)
	}
	return *res, nil

}

func buildRpcStringToSign(request requests.AcsRequest) (stringToSign string) {
	signParams := make(map[string]string)
	sortSignParams := make(map[string]string)
	sortQuerySlice := make([]string, 0, 10)

	//对请求参数按参数名排序
	for key, value := range request.GetQueryParams() {
		sortQuerySlice = append(sortQuerySlice, key)
		signParams[key] = value
	}
	for key, value := range request.GetFormParams() {
		sortQuerySlice = append(sortQuerySlice, key)
		signParams[key] = value
	}
	for key, v := range signParams {
		key := key
		sortSignParams[key] = v
	}

	stringToSign = aliUtils.GetUrlFormedMap(sortSignParams)
	stringToSign = strings.Replace(stringToSign, "+", "%20", -1)
	stringToSign = strings.Replace(stringToSign, "*", "%2A", -1)
	stringToSign = strings.Replace(stringToSign, "%7E", "~", -1)
	stringToSign = url.QueryEscape(stringToSign)
	//stringToSign = strings.Replace(stringToSign, "%26", "&", -1)
	stringToSign = request.GetMethod() + "&%2F&" + stringToSign
	return
}

func completeRpcSignParams(request requests.AcsRequest, signer aliAuth.Signer, regionId string) (err error) {
	queryParams := request.GetQueryParams()
	queryParams["Version"] = request.GetVersion()
	queryParams["Action"] = request.GetActionName()
	queryParams["Format"] = request.GetAcceptFormat()
	queryParams["Timestamp"] = hookGetDate(GetTimeInFormatISO8601)
	queryParams["SignatureMethod"] = signer.GetName()
	queryParams["SignatureVersion"] = signer.GetVersion()
	queryParams["SignatureNonce"] = hookGetNonce(GetUUID)
	queryParams["AccessKeyId"], err = signer.GetAccessKeyId()

	if err != nil {
		return
	}

	if extraParam := signer.GetExtraParam(); extraParam != nil {
		for key, value := range extraParam {
			queryParams[key] = value
		}
	}

	if request.GetMethod() == http.MethodPost {
		request.GetHeaders()["Content-Type"] = requests.Form
		formString := GetUrlFormedMap(request.GetFormParams())
		request.SetContent([]byte(formString))
	}
	return
}

func GetUrlFormedMap(source map[string]string) (urlEncoded string) {
	urlEncoder := url.Values{}
	for key, value := range source {
		urlEncoder.Add(key, value)
	}
	urlEncoded = urlEncoder.Encode()
	return
}

func RandStringBytes(n int) string {
	b := make([]byte, n)
	for i := range b {
		b[i] = consts.LetterBytes[rand2.Intn(len(consts.LetterBytes))]
	}
	return string(b)
}

func GetUUID() (uuidHex string) {
	uuid := NewUUID()
	uuidHex = hex.EncodeToString(uuid[:])
	return
}

func NewUUID() UUID {
	ns := UUID{}
	safeRandom(ns[:])
	u := newFromHash(md5.New(), ns, RandStringBytes(16))
	u[6] = (u[6] & 0x0f) | (byte(2) << 4)
	u[8] = (u[8]&(0xff>>2) | (0x02 << 6))

	return u
}

type UUID [16]byte

func safeRandom(dest []byte) {
	if _, err := rand.Read(dest); err != nil {
		panic(err)
	}
}

func newFromHash(h hash.Hash, ns UUID, name string) UUID {
	u := UUID{}
	h.Write(ns[:])
	h.Write([]byte(name))
	copy(u[:], h.Sum(nil))

	return u
}

func GetTimeInFormatISO8601() (timeStr string) {
	gmt := time.FixedZone("GMT", 0)

	return time.Now().In(gmt).Format("2006-01-02T15:04:05Z")
}

func ShaHmac1(source, secret string) string {
	key := []byte(secret)
	hmac := hmac.New(sha1.New, key)
	hmac.Write([]byte(source))
	signedBytes := hmac.Sum(nil)
	signedString := base64.StdEncoding.EncodeToString(signedBytes)
	return signedString
}

// Strval 获取变量的字符串值
// 浮点型 3.0将会转换成字符串3, "3"
// 非数值或字符类型的变量将会被转换成JSON格式字符串
func Strval(value interface{}) string {
	var key string
	if value == nil {
		return key
	}

	switch value.(type) {
	case float64:
		ft := value.(float64)
		key = strconv.FormatFloat(ft, 'f', -1, 64)
	case float32:
		ft := value.(float32)
		key = strconv.FormatFloat(float64(ft), 'f', -1, 64)
	case int:
		it := value.(int)
		key = strconv.Itoa(it)
	case uint:
		it := value.(uint)
		key = strconv.Itoa(int(it))
	case int8:
		it := value.(int8)
		key = strconv.Itoa(int(it))
	case uint8:
		it := value.(uint8)
		key = strconv.Itoa(int(it))
	case int16:
		it := value.(int16)
		key = strconv.Itoa(int(it))
	case uint16:
		it := value.(uint16)
		key = strconv.Itoa(int(it))
	case int32:
		it := value.(int32)
		key = strconv.Itoa(int(it))
	case uint32:
		it := value.(uint32)
		key = strconv.Itoa(int(it))
	case int64:
		it := value.(int64)
		key = strconv.FormatInt(it, 10)
	case uint64:
		it := value.(uint64)
		key = strconv.FormatUint(it, 10)
	case string:
		key = value.(string)
	case []byte:
		key = string(value.([]byte))
	default:
		newValue, _ := json.Marshal(value)
		key = string(newValue)
	}

	return key
}
