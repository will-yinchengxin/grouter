package request

type Trans struct {
}

type BasisInfo struct {
	UUID             string `form:"uuid" validate:"required,min=1"`
	UserID           int    `form:"userId" validate:"required,gt=0"`
	ClientAppVersion string `form:"clientAppVersion" validate:"required,min=1"`
	SessionID        string `form:"sessionId" validate:"required,min=1"`
}

type TransReq struct {
	BasisInfo
	ApiName         string `form:"apiName" validate:"required,oneof=RunInstance StopInstance"`
	ResourceId      string `form:"resourceId" validate:"omitempty,min=1"`
	AppPackage      string `form:"appPackage" validate:"required,min=1"`
	AppActivityName string `form:"appActivityName" validate:"omitempty,min=1"`
}

type TransReqToApi struct {
	Url             string `json:"url" comment:"请求地址不带参数"`
	Method          string `json:"method" comment:"请求类型"`
	Domain          string `json:"domain" comment:"域名"`
	ApiName         string `json:"api_name" comment:"方法"`
	Version         string `json:"version" comment:"方法"`
	Scheme          string `json:"scheme" comment:""`
	UrlData         string `json:"url_data" comment:"query参数"`
	FormData        string `json:"form_data" comment:"form参数"`
	AccessKeyId     string `json:"access_key_id" comment:"access_key_id"`
	AccessKeySecret string `json:"access_key_secret" comment:"access_key_secret"`
}

type HeartBeatReq struct {
	BasisInfo
}
