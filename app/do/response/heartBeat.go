package response

type HeartBeatRep struct {
	Status int64  `json:"status"`
	UUid   string `json:"uuid"`
	UserId string `json:"userId"`
}
