package response

type CloudInstanceInfo struct {
	DeviceSpec    string `json:"DeviceSpec"`
	Architecture  string `json:"Architecture"`
	IP            string `json:"Ip"`
	Token         string `json:"Token"`
	ID            string `json:"Id"`
	Port1         int    `json:"Port1"`
	AppLaunchInfo struct {
		AppVersion string `json:"AppVersion"`
		AppPackage string `json:"AppPackage"`
	} `json:"AppLaunchInfo"`
}

type TransResRemote struct {
	Model struct {
		Type        string `json:"Type"`
		CommandInfo struct {
			Action string `json:"Action"`
		} `json:"CommandInfo"`
		CloudInstanceInfo `json:"CloudInstanceInfo"`
		RequestID         string `json:"RequestId"`
		ExtInfo           struct {
		} `json:"ExtInfo"`
	} `json:"Model"`
	Success bool `json:"Success"`
}

type TransRep struct {
	UrlData           string `json:"urlData"`
	SessionID         string `json:"session_id"`
	CloudInstanceInfo `json:"CloudInstanceInfo"`
}
