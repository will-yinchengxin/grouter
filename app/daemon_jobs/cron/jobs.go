package cron

import (
	"context"
	"github.com/sirupsen/logrus"
	"time"
	"will/app/modules/mysql/dao"
	conn "will/app/modules/redis"
	"will/core"
	"will/will_tools/logs"
)

type Jobs struct {
	Rds             *conn.RedisPool
	Log             *dao.Log
	Resource        *dao.Resource
	ClientHeartBeat *dao.ClientHeartBeat
}

func (j *Jobs) RegisterJobs() func() {
	defer func() {
		if err := recover(); err != nil {
			logInfo := logs.TraceFormatter{
				Trace: logrus.Fields{
					"error": err,
				},
			}
			_ = core.Log.Panic(logInfo)
		}
	}()
	ctx, cancel := context.WithCancel(context.Background())
	go j.startCronJobs(ctx)
	return func() {
		cancel()
	}
}

func (j *Jobs) startCronJobs(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			closeLog := logs.StringFormatter{
				Msg: "Receiving Exit Signal Of Main Program，Stop All The CronJobs Success",
			}
			_ = core.Log.Success(closeLog)
			return
		case <-time.Tick(30 * time.Second):
			j.transCronJob()
		}
	}
}
