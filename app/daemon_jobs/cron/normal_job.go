package cron

import (
	"encoding/json"
	"strconv"
	"time"
	requestParam "will/app/do/request"
	response2 "will/app/do/response"
	"will/app/service"
	"will/consts"
	"will/core"
)

func (j *Jobs) transCronJob() {
	var (
		offset    = 0
		batchSize = 1000
	)
	count, err := j.ClientHeartBeat.GetCount()
	if err != nil {
		_ = core.Log.ErrorDefault(err.Error())
		return
	}
	for {
		go func(offsetNew, batchSizeNew int) {
			list, err := j.ClientHeartBeat.GetList(offsetNew, batchSizeNew)
			if err != nil {
				_ = core.Log.ErrorDefault("TransCronJob GetList Err: " + err.Error())
				return
			}
			for _, val := range list {
				timeStr := j.Rds.Get(val.Uuid + val.SessionId)
				// 记录过期，删除记录 释放资源
				if len(timeStr) == 0 && time.Now().Unix()-val.CreateTime > consts.TimeInterVal {
					tmp := response2.TransRep{}
					json.Unmarshal([]byte(val.SucData), &tmp)
					j.Resource.Delete(tmp.UrlData, val.SessionId)
					j.ClientHeartBeat.Delete(val.ID)
					// 释放资源
					tmpReq := requestParam.TransReq{}
					tmpReq.UUID = val.Uuid
					UserID, _ := strconv.Atoi(val.Userid)
					tmpReq.UserID = UserID
					tmpReq.ClientAppVersion = val.ClientAppVersion
					tmpReq.ApiName = consts.StopIns
					tmpReq.ResourceId = val.ResourceId
					tmpReq.SessionID = val.SessionId
					tmpReq.AppPackage = val.AppPackage
					if _, err = service.PrepareTransApi(tmpReq); err != nil {
						_ = core.Log.ErrorDefault("TransCronJob PrepareTransApi Err: " + err.Error())
					}
					time.Sleep(time.Millisecond * 200)
				}
				// 有心跳
				if len(timeStr) != 0 {
					timeInt, err := strconv.Atoi(timeStr)
					// 更细一下心跳
					err = j.ClientHeartBeat.Update(val, int64(timeInt))
					if err != nil {
						_ = core.Log.ErrorDefault("TransCronJob Update HearBeat Err: " + err.Error())
						return
					}
				}
				time.Sleep(time.Millisecond * 500)
			}
		}(offset, batchSize)
		offset += batchSize
		if offset >= int(count) {
			break
		}
	}
	return
}
