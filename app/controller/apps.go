package controller

import (
	"context"
	"github.com/gin-gonic/gin"
	"will/app/do/request"
	"will/app/service"
	"will/utils"
	"will/utils/validator"
)

type Apps struct {
	RequestValidate *validator.ValidatorX
	App             service.Apps
}

func (apps *Apps) TransApi(ctx *gin.Context) {
	var (
		req           request.TransReq
		spanFatherCtx context.Context
	)
	if errMsg := apps.RequestValidate.ParseQuery(ctx, &req); errMsg != "" {
		utils.MessageError(ctx, errMsg)
		return
	}
	spanFatherCtx = context.Background()
	res, codeType := apps.App.TransApi(spanFatherCtx, req)
	if codeType.Code != 0 {
		utils.Error(ctx, codeType)
		return
	}
	utils.Out(ctx, res)
	return
}

func (apps *Apps) HeartBeat(ctx *gin.Context) {
	var (
		req           request.HeartBeatReq
		spanFatherCtx context.Context
	)
	if errMsg := apps.RequestValidate.ParseQuery(ctx, &req); errMsg != "" {
		utils.MessageError(ctx, errMsg)
		return
	}
	spanFatherCtx = context.Background()
	codeType := apps.App.HeartBeat(spanFatherCtx, req)
	if codeType.Code != 0 {
		utils.Error(ctx, codeType)
		return
	}
	utils.Success(ctx)
	return
}
