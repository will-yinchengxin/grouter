/*
 Source Server Type    : MySQL
 Source Schema         : grouter

 Date: 22/03/2023 18:32:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for client_heart_beat
-- ----------------------------
DROP TABLE IF EXISTS `client_heart_beat`;
CREATE TABLE `client_heart_beat` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `user_id` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `client_app_version` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `session_id` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `api_name` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `resource_id` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '',
  `create_time` int(11) NOT NULL DEFAULT '0',
  `suc_data` text,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态: 1: 使用 2释放',
  `app_package` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_uid_unique` (`uuid`,`user_id`,`session_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for log
-- ----------------------------
DROP TABLE IF EXISTS `log`;
CREATE TABLE `log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `req_time` bigint(13) NOT NULL DEFAULT '0' COMMENT '请求的时间',
  `req_type` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '' COMMENT '请求类型',
  `req_param` text CHARACTER SET latin1 NOT NULL COMMENT '请求消息',
  `ret_status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '请求状态: 1 成功 2 失败',
  `ret_msg` varchar(5000) CHARACTER SET latin1 NOT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Table structure for resource
-- ----------------------------
DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `token` varchar(1000) NOT NULL DEFAULT '',
  `session_id` varchar(1000) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

SET FOREIGN_KEY_CHECKS = 1;
