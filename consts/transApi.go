package consts

const (
	ReqSuc = iota + 1
	ReqFail

	LetterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
	RunIns      = "RunInstance"
	StopIns     = "StopInstance"
	TranUser    = "admin"

	TimeInterVal = 120
)
