package config

import (
	"github.com/spf13/viper"
	"os"
	"will/consts"
)

type Setting struct {
	vp *viper.Viper
}

func NewSetting(env string) (*Setting, error) {
	vp := viper.New()
	if PathExists(consts.ConfigPath) {
		vp.AddConfigPath(consts.ConfigPath)
	} else {
		vp.AddConfigPath(consts.ConfigLocalPath)
	}
	vp.SetConfigName(env + consts.ConfigName)
	vp.SetConfigType(consts.ConfigType)
	err := vp.ReadInConfig()
	if err != nil {
		return nil, err
	}
	return &Setting{vp}, nil
}

func (s *Setting) ReadSection(k string, v interface{}) error {
	err := s.vp.UnmarshalKey(k, v)
	if err != nil {
		return err
	}
	return nil
}

func PathExists(path string) bool {
	_, err := os.Stat(path)
	if err == nil {
		return true
	}
	if os.IsNotExist(err) {
		return false
	}
	return false
}
